const path = require('path')
const tape = require('tape')

const { Diorama, tapeExecutor, backwardCompatibilityMiddleware } = require('@holochain/diorama')

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.error('got unhandledRejection:', error);
});

const dnaPath = path.join(__dirname, "../dist/holoask.dna.json")
const dna = Diorama.dna(dnaPath, 'holoask')

const diorama = new Diorama({
  instances: {
    alice: dna,
    bob: dna,
  },
  bridges: [],
  debugLog: false,
  executor: tapeExecutor(require('tape')),
  middleware: backwardCompatibilityMiddleware,
})

diorama.registerScenario("description of example test", async (s, t, { alice }) => {
  // Make a call to a Zome function
  // indicating the function, and passing it an input
  const addrQ = await alice.call("questions", "create_question", {"content":"sample question", "timestamp": 1})
  const addrA = await alice.call("questions", "create_answer", {"content":"sample answer", "timestamp": 2, "question": addrQ.Ok})
  const resultQ = await alice.call("questions", "get_my_entry", {"address": addrQ.Ok})
  const resultA = await alice.call("questions", "get_my_entry", {"address": addrA.Ok})

  // check for equality of the actual and expected results
  t.deepEqual(resultQ, { Ok: { App: [ 'question', '{"author":"HcScjwO9ji9633ZYxa6IYubHJHW6ctfoufv5eq4F7ZOxay8wR76FP4xeG9pY3ui","content":"sample question","timestamp":1}' ] } })
  t.deepEqual(resultA, { Ok: { App: [ 'answer', '{"author":"HcScjwO9ji9633ZYxa6IYubHJHW6ctfoufv5eq4F7ZOxay8wR76FP4xeG9pY3ui","content":"sample answer","timestamp":2,"question":"' + addrQ.Ok + '"}' ] } })
})

diorama.run()
