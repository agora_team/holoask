#![feature(proc_macro_hygiene)]
#[macro_use]
extern crate hdk;
extern crate hdk_proc_macros;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
#[macro_use]
extern crate holochain_json_derive;

use hdk::{
    //LinkMatch,
    AGENT_ADDRESS,
    entry_definition::ValidatingEntryType,
    error::ZomeApiResult,
};
use hdk::holochain_core_types::{
    entry::Entry,
    dna::entry_types::Sharing,
};

use hdk::holochain_json_api::{
    json::JsonString,
    error::JsonError
};

use hdk::holochain_persistence_api::{
    cas::content::Address
};

use hdk_proc_macros::zome;

// see https://developer.holochain.org/api/latest/hdk/ for info on using the hdk library

// This is a sample zome that defines an entry type "MyEntry" that can be committed to the
// agent's chain via the exposed function create_my_entry

#[derive(Serialize, Deserialize, Debug, DefaultJson,Clone)]
pub struct Question {
    author: Address,
    content: String,
    timestamp: i128,
}

#[derive(Serialize, Deserialize, Debug, DefaultJson,Clone)]
pub struct Answer {
    author: Address,
    content: String,
    timestamp: i128,
    question: Address,
}

#[zome]
mod questions_zome {

    #[init]
    fn init() {
        Ok(())
    }

    #[validate_agent]
    pub fn validate_agent(validation_data: EntryValidationData<AgentId>) {
        Ok(())
    }

    #[entry_def]
    fn question_def() -> ValidatingEntryType {
        entry!(
            name: "question",
            description: "this is a same entry definition",
            sharing: Sharing::Public,
            validation_package: || {
                hdk::ValidationPackageDefinition::Entry
            },
            validation: | validation_data: hdk::EntryValidationData<Question>| {
                match validation_data {
                    hdk::EntryValidationData::Create{entry, validation_data: data} => {
                        if entry.content.len() > 280 {
                            return Err("This is not the Bible".into())
                        }
                        for source in data.sources() {
                            if source == entry.author {
                                return Ok(());
                            }
                        }
                        Err("Identity theft is a crime".into())
                    },
                    _ => {
                        Err("Questions can not be deleted or modified.".into())
                    }
                }
            }
        )
    }

    #[entry_def]
    fn answer_def() -> ValidatingEntryType {
        entry!(
            name: "answer",
            description: "this is a same entry definition",
            sharing: Sharing::Public,
            validation_package: || {
                hdk::ValidationPackageDefinition::Entry
            },
            validation: | validation_data: hdk::EntryValidationData<Answer>| {
                match validation_data {
                    hdk::EntryValidationData::Create{entry, validation_data: data} => {
                        if entry.content.len() > 4000 {
                            return Err("Cool story bro, but try making it shorter".into())
                        }
                        for source in data.sources() {
                            if source == entry.author {
                                return Ok(());
                            }
                        }
                        Err("What a copycat".into())
                    },
                    _ => {
                        Err("Answers can not be deleted or modified.".into())
                    }
                }
            },
            links: [
                from!(
                    "question",
                    link_type: "question_to_answer",
                    validation_package: || {
                        hdk::ValidationPackageDefinition::ChainFull
                    },
                    validation: | _validation_data: hdk::LinkValidationData | {
                        Ok(())
                    }
                ),
                from!(
                    "%agent_id",
                    link_type: "vote",
                    validation_package: || {
                        hdk::ValidationPackageDefinition::ChainFull
                    },
                    validation: | validation_data: hdk::LinkValidationData | {
                        if let hdk::LinkValidationData::LinkAdd{link, ..} = validation_data {
                            if link.link.tag() == "+" || link.link.tag() == "-" {
                                Ok(())
                            } else {
                                Err("You are not a god".into())
                            }
                        } else {
                            Err("This vote is bullshit".into())
                        }
                    }
                ),
                from!(
                    "%agent_id",
                    link_type: "selected_answer",
                    validation_package: || {
                        hdk::ValidationPackageDefinition::ChainFull
                    },
                    validation: | validation_data: hdk::LinkValidationData | {
                        if let hdk::LinkValidationData::LinkAdd{link, ..} = validation_data {
                            let provenances = link.top_chain_header.provenances().clone();
                            if provenances.len() != 1 {
                                Err("Nice try bitch".into())
                            } else if link.link.base().clone() == provenances[0].source() {
                                Ok(())
                            } else {
                                Err("You cheeky bastard".into())
                            }
                        } else {
                            Err("A man needs a name".into())
                        }
                    }
                )
            ]
        )
    }

    #[zome_fn("hc_public")]
    fn create_question(content: String, timestamp: i128) -> ZomeApiResult<Address> {
        let question = Question{content, timestamp, author: AGENT_ADDRESS.clone()};
        let entry = Entry::App("question".into(), question.into());
        let address = hdk::commit_entry(&entry)?;
        Ok(address)
    }

    #[zome_fn("hc_public")]
    fn create_answer(content: String, timestamp: i128, question: Address) -> ZomeApiResult<Address> {
        let answer = Answer{content, timestamp, question: question.clone(), author: AGENT_ADDRESS.clone()};
        let entry = Entry::App("answer".into(), answer.into());
        let address = hdk::commit_entry(&entry)?;
        hdk::link_entries(&question, &address, String::from("question_to_answer"), String::from(""))?;
        Ok(address)
    }

/* Fuck this shit
    #[zome_fn("hc_public")]
    fn get_question_answers(question: Address) -> ZomeApiResult<Vec<Address>> {
        Ok(hdk::get_links(&question, LinkMatch::Exactly("question_to_answer"), LinkMatch::Any).addresses())
    }
*/

    #[zome_fn("hc_public")]
    fn get_my_entry(address: Address) -> ZomeApiResult<Option<Entry>> {
        hdk::get_entry(&address)
    }
}
